import java.util.Arrays;
import java.util.stream.Stream;

public class GenerationNumberProduct {
    public void sortedMass(String numbersProject){
        Stream<String> streamFromString = Stream.of(numbersProject.split(" "));
        int[] intNumbersProject = streamFromString.mapToInt(Integer::parseInt).sorted().toArray();
        System.out.println(generateNumber(intNumbersProject));
    }
    public int generateNumber(int[] numbersProject){
        int a ;
        int generatedNumberProject = 0;
        for(int i = 1; i < numbersProject.length; i++) {
            a = numbersProject[i-1] - numbersProject[i];
            if(a < -1){
                generatedNumberProject =  numbersProject[i-1] + 1;
            }
        }
        if(generatedNumberProject == 0){
            generatedNumberProject = numbersProject[numbersProject.length -1] +1;
        }
       return generatedNumberProject;
    }
}
